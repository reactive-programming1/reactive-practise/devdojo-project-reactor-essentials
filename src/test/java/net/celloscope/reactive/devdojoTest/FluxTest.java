package net.celloscope.reactive.devdojoTest;

import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.reactivestreams.Subscriber;
import org.reactivestreams.Subscription;
import reactor.core.publisher.BaseSubscriber;
import reactor.core.publisher.ConnectableFlux;
import reactor.core.publisher.Flux;
import reactor.test.StepVerifier;
import reactor.util.context.Context;

import java.time.Duration;
import java.util.List;
import java.util.Map;

@Slf4j
public class FluxTest {

    @Test
    void FluxSubscriber(){
        Flux<String> fluxString = Flux.just("ere","gdfv","hnytj");

        StepVerifier.create(fluxString.log())
                .expectNext("ere","gdfv","hnytj")
                .verifyComplete();


        Flux<Integer> fluxInteger = Flux.range(1,9);

        StepVerifier.create(fluxInteger.log())
                .expectNextCount(9)
                .verifyComplete();
    }


    @Test
    void fluxFromIterable(){

        Flux<Integer> fluxInteger = Flux.fromIterable(List.of(1,2,3,4,5,6,7,8,9));

        fluxInteger.subscribe(p->log.info("printing values : {}",p),Throwable::printStackTrace,()->log.info("Runnable Completed!!"));

        StepVerifier.create(fluxInteger.log())
                .expectNextCount(9)
                .verifyComplete();
    }


    @Test
    void fluxFromIterableError(){
        Flux<Integer> fluxIterableError = Flux.range(1,9)
                .map(i->{
                    if (i==7) throw new IndexOutOfBoundsException();
                    return i;
                });

        fluxIterableError.subscribe(p->log.info("printing values : {}",p),Throwable::printStackTrace,()->log.info("Runnable Completed!!"));

        StepVerifier.create(fluxIterableError.log())
                .expectNextCount(6)
                .expectError(IndexOutOfBoundsException.class)
                .verify();

    }


    @Test
    void fluxFromIterableErrorAvoid(){
        Flux<Integer> fluxIterableError = Flux.range(1,9)
                .map(i->{
                    if (i==7) throw new IndexOutOfBoundsException();
                    return i;
                });

        // HOW TO AVOID ERROR
        // HOW TO REQUEST FOR SPECIFIC ELEMENTS TO THIS PUBLISHER
        fluxIterableError.subscribe(p->log.info("printing values : {}",p),e->e.getMessage(),()->log.info("Runnable Completed!!"));

        StepVerifier.create(fluxIterableError.log())
                .expectNextCount(6)
                .expectError(IndexOutOfBoundsException.class)
                .verify();

    }


    @Test
    void fluxSubscriberImplementsBackPressure(){
        Flux<Integer> fluxBackpressureImplement = Flux.range(1,9).log();


        fluxBackpressureImplement.subscribe(new Subscriber<>() {
            private Subscription subscription;
            private final int limit = 2;
            private int count = 0;
            @Override
            public void onSubscribe(Subscription subscription) {
                this.subscription=subscription;
                // implement backpressure with requested number
                subscription.request(limit);
            }

            @Override
            public void onNext(Integer integer) {
                count++;
                if (count>=limit){
                    count=0;
                    subscription.request(limit);
                }
            }

            @Override
            public void onError(Throwable throwable) {

            }

            @Override
            public void onComplete() {

            }
        });

        StepVerifier.create(fluxBackpressureImplement)
                .expectNextCount(9)
                .verifyComplete();

    }

    @Test
    void fluxSubscriberImplementsBackPressureModified() {
        Flux<Integer> fluxBackpressureImplementImproved = Flux.range(1, 9).log();
        fluxBackpressureImplementImproved.subscribe(new BaseSubscriber<Integer>() {
            private final int limit = 2;
            private int count = 0;
            @Override
            protected void hookOnSubscribe(Subscription subscription) {
                super.hookOnSubscribe(subscription);
                request(limit);
            }

            @Override
            protected void hookOnNext(Integer value) {
                count++;
                if (count>=limit){
                    count=0;
                    request(limit);
                }
            }
        });
        StepVerifier.create(fluxBackpressureImplementImproved)
                .expectNextCount(9)
                .verifyComplete();
    }


    @Test
    void fluxSubscriberImplementsBackPressureLimitRate() {
        Flux<Integer> fluxBackpressureImplementLimitRate = Flux.range(1, 9).log().limitRate(3);

        fluxBackpressureImplementLimitRate.subscribe(s->log.info("emitted value = {}",s));

        StepVerifier.create(fluxBackpressureImplementLimitRate)
                .expectNextCount(9)
                .verifyComplete();
    }


    @Test
    void fluxTypeConnectable() {
        ConnectableFlux<Integer> connectableFlux = Flux.range(1, 10).log()
                .delayElements(Duration.ofMillis(1000))
                .publish();

        connectableFlux.connect();

        connectableFlux.subscribe(i->log.info("Subscriber 1 : {}",i));

//        StepVerifier.create(connectableFlux)
//                .expectNextCount(9)
//                .verifyComplete();
    }


    @Test
    void fluxTypeConnectableAutoConnect() {
        Flux<Integer> connectableFluxAuto = Flux.range(1, 10).log()
                .delayElements(Duration.ofMillis(1000))
                .publish()
                .autoConnect(2);



        StepVerifier.create(connectableFluxAuto) // 1st subscriber
                .then(connectableFluxAuto::subscribe) // 2nd subscriber
                .expectNext(1,2,3,4,5,6,7,8,9,10)
                .expectComplete()
                .verify();
    }






}
