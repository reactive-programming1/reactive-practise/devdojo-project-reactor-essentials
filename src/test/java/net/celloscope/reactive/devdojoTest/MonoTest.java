package net.celloscope.reactive.devdojoTest;

import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.reactivestreams.Subscription;
import reactor.core.publisher.Mono;
import reactor.test.StepVerifier;

@Slf4j
public class MonoTest {

    @Test
    public void monoSubscriber() {
        String myName = "My name is SAJAL";
        Mono<String> publisherMono = Mono.just(myName).log();

        // Subscribe this MONO publisher
//        publisherMono.subscribe();

        log.info("Trace of Mono Publisher {}", publisherMono.log());


        StepVerifier.create(publisherMono)
                .expectNext("My name is SAJAL")
                .verifyComplete();
    }

    @Test
    public void monoSubscriberMethodOverloading() {
        String myName = "My name is SAJAL";
        Mono<String> publisherMono = Mono.just(myName);

        // suppose We have this subscriber that logs the value
        // here method subscriber() overloaded with logging
        publisherMono.subscribe(s -> log.info("value: {}", s));

        StepVerifier.create(publisherMono)
                .expectNext(myName)
                .verifyComplete();
    }

    @Test
    public void mono_Subscribe_Method_Implements_Consumer_Interface_Overloading_Example_with_Error() {
        String myName = "My name is SAJAL";
        Mono<String> publisherMono = Mono.just(myName)

                .map(m -> {
                    throw new RuntimeException("Error Happened");
                });

        // suppose We have this subscriber that logs the value
        // here subscribe method subscriber() overloaded with a Sting super class and Throwable super class
        publisherMono.subscribe(s -> log.info("If no Error occurs: {}", s), s -> log.error("works when any error happens"));

        // just incase anyone wants to see the full stack trace there we can print it out
        publisherMono.subscribe(s -> log.info("If no Error occurs: {}", s), Throwable::printStackTrace);

        StepVerifier.create(publisherMono)
                .expectError(RuntimeException.class)
                .verify();
    }


    @Test
    public void mono_Subscribe_Method_Implementation_with_Three_Interfaces() {
        String myName = "mohammad abu yousuf sajal";
        Mono<String> publisherMono = Mono.just(myName)
                .map(String::toUpperCase);

        // suppose We have this subscriber that logs the value
        // here subscribe method subscriber() overloaded with a Sting super class and Throwable super class
        publisherMono.subscribe(s -> log.info("The output: {}", s), s -> log.error("works when any error happens"), () -> log.info("Successfully executed!"));

        StepVerifier.create(publisherMono)
                .expectNext(
                        myName.toUpperCase()
                )
                .verifyComplete();
    }


    @Test
    public void mono_Subscribe_Method_Implementation_with_Four_Interfaces() {
        String myName = "mohammad abu yousuf sajal";
        Mono<String> publisherMono = Mono.just(myName)
                .map(String::toUpperCase).log();

        // suppose We have this subscriber that logs the value
        // here subscribe method implements new Subscription superclass
        // the structure is
        // @Nullable Consumer<? super T> consumer, @Nullable Consumer<? super Throwable> errorConsumer, @Nullable Runnable completeConsumer, @Nullable Consumer<? super Subscription> subscriptionConsumer
        publisherMono.subscribe(s -> log.info("The output: {}", s), s -> log.error("works when any error happens"), () -> log.info("Successfully executed!"), Subscription::cancel);

        StepVerifier.create(publisherMono)
                .expectNext(
                        myName.toUpperCase()
                )
                .verifyComplete();


        // another example of Subscription class behaviour
        publisherMono.subscribe(s -> log.info("The output: {}", s), s -> log.error("works when any error happens"), () -> log.info("Successfully executed!"), s -> s.request(2));
//        publisherMono.log();
    }


    @Test
    public void mono_Publisher_DoOn_Methods() {
        String myName = "MD ABU YOUSUF SAJAL";
        Mono<String> monoPublisher = Mono.just(myName)
                .log()
                .map(String::toLowerCase)
                .doOnSubscribe(subscription -> log.info("Do onSubscribe Method"))
                .doOnRequest(c -> log.info("Do On Request"))
                .doOnNext(d -> log.info("Get the value {}", d))
                .doOnSuccess(s -> log.info("Successfully Completed!"));
        monoPublisher.subscribe(s -> log.info("Data {}", s));
    }

    @Test
    public void mono_Publisher_DoOn_Error_Methods() {
        String myName = "MD ABU YOUSUF SAJAL";
        Mono<Object> error = Mono.error(new IllegalArgumentException())
                .log()
                .doOnError(e -> log.error("Error Stack {}", e))
                .doOnNext(s -> log.info("DON ON NEXT executing"));

        StepVerifier.create(error)
                .expectError(IllegalArgumentException.class)
                .verify();
    }

    @Test
    public void mono_Publisher_DoOn_Error_Resume() {
        Mono<Object> error = Mono.error(new IllegalArgumentException())
                .doOnError(e -> log.info("Exception happened = {}", e.getMessage())) //this line didn't execute because doOnErrorReume will execute before doOnError. So if we closely monitor this issues we can check that, doOnError had IllegalArgumentException and doOnErrorResume has RuntimeException. Finally, RuntimeException occurred.
                .onErrorResume(r -> {
                            log.info("On Error now it is calling onErrorResume");
                            return Mono.error(new RuntimeException());
                        }
                ).log();

        StepVerifier.create(error)
                .expectError(RuntimeException.class)
                .verify();
    }


    @Test
    public void mono_Publisher_DoOn_Error_Return() {
        Mono<Object> error = Mono.error(new IllegalArgumentException())
                .doOnError(e -> log.info("Exception happened = {}", e.getMessage()))
                .onErrorResume(r -> {
                            log.info("On Error now it is calling onErrorResume");
                            return Mono.error(new RuntimeException());
                        }
                )
                //this method onErrorReturn will execute only. because above those methods are obsolete when this method implemented. Hence, we need to expect any Fallback value returning by this method.
                .onErrorReturn("ON ERROR this message is Returning!")
                .log();

        StepVerifier.create(error)
                .expectNext("ON ERROR this message is Returning!")
                .verifyComplete();
    }

}
