package net.celloscope.reactive.devdojoTest;

import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.core.scheduler.Schedulers;
import reactor.test.StepVerifier;

import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import java.util.concurrent.atomic.AtomicLong;


@Slf4j
public class OperatorsTest {

    // subscribeOn effects all the publisher no matter where it is called
    @Test
    void subscribeOnSingleThread() {
        Flux<Integer> subscribeOnPublisher = Flux.range(1, 5)
                .subscribeOn(Schedulers.single())
                .map(i -> {
                    log.info("printing from map-1, thread == {}", Thread.currentThread().getName());
                    return i;
                })
//                .subscribeOn(Schedulers.single())
                .map(i -> {
                    log.info("printing from map-2, thread == {}", Thread.currentThread().getName());
                    return i;
                })
//                .subscribeOn(Schedulers.single())
                ;


        StepVerifier.create(subscribeOnPublisher)
                .expectNextCount(5)
                .verifyComplete();
    }

    // subscribeOn effects all the publisher no matter where it is called
    @Test
    void subscribeOnMultiThread() {
        Flux<Integer> subscribeOnPublisher = Flux.range(1, 5)
                .subscribeOn(Schedulers.boundedElastic())
                .map(i -> {
                    log.info("printing from map-1, thread == {}", Thread.currentThread().getName());
                    return i;
                })
//                .subscribeOn(Schedulers.single())
                .map(i -> {
                    log.info("printing from map-2, thread == {}", Thread.currentThread().getName());
                    return i;
                })
//                .subscribeOn(Schedulers.single())
                ;


        StepVerifier.create(subscribeOnPublisher)
                .expectNextCount(5)
                .verifyComplete();
    }


    // publishOn effects only after methods
    @Test
    void publishOnMethod() {
        Flux<Integer> subscribeOnPublisher = Flux.range(1, 5)
//                .publishOn(Schedulers.boundedElastic())
                .map(i -> {
                    log.info("printing from map-1, thread == {}", Thread.currentThread().getName());
                    return i;
                })
                //after this the process will run on another thread
                .publishOn(Schedulers.boundedElastic())
                .map(i -> {
                    log.info("printing from map-2, thread == {}", Thread.currentThread().getName());
                    return i;
                });

        StepVerifier.create(subscribeOnPublisher)
                .expectNextCount(5)
                .verifyComplete();
    }

    @Test
    void multipleSubscribeOnMethod() {
        Flux<Integer> subscribeOnPublisher = Flux.range(1, 5)
                //this will be affected on all the publisher's subscription. Because it is declared earlier.
                .subscribeOn(Schedulers.boundedElastic())
                .map(i -> {
                    log.info("printing from map-1, thread == {}", Thread.currentThread().getName());
                    return i;
                })
                .subscribeOn(Schedulers.single())
                .map(i -> {
                    log.info("printing from map-2, thread == {}", Thread.currentThread().getName());
                    return i;
                });


        StepVerifier.create(subscribeOnPublisher)
                .expectNextCount(5)
                .verifyComplete();
    }


    @Test
    void multiplePublishOnMethod() {

        Flux<Integer> subscribeOnPublisher = Flux.range(1, 5)
                //map-1 will execute on single thread
                .publishOn(Schedulers.single())
                .map(i -> {
                    log.info("printing from map-1, thread == {}", Thread.currentThread().getName());
                    return i;
                })
                //map-2 will execute on boundedElastic thread
                .publishOn(Schedulers.boundedElastic())
                .map(i -> {
                    log.info("printing from map-2, thread == {}", Thread.currentThread().getName());
                    return i;
                });

        StepVerifier.create(subscribeOnPublisher)
                .expectNextCount(5)
                .verifyComplete();

    }


    @Test
    void combinedSubscribeOnAndPublishOnMethod() {

        Flux<Integer> subscribeOnPublisher = Flux.range(1, 5)

                //publishOn will dominant here. it will force every publisher to run on Single thread
                .publishOn(Schedulers.single())
                .map(i -> {
                    log.info("printing from map-1, thread == {}", Thread.currentThread().getName());
                    return i;
                })

                .subscribeOn(Schedulers.boundedElastic())
                .map(i -> {
                    log.info("printing from map-2, thread == {}", Thread.currentThread().getName());
                    return i;
                });

        StepVerifier.create(subscribeOnPublisher)
                .expectNextCount(5)
                .verifyComplete();

    }


    @Test
    void combinedSubscribeOnAndPublishOnMethod_2() {

        Flux<Integer> subscribeOnPublisher = Flux.range(1, 5)

                // subscribeOn will force map-1 to run in single thread
                .subscribeOn(Schedulers.boundedElastic())
                .map(i -> {
                    log.info("printing from map-1, thread == {}", Thread.currentThread().getName());
                    return i;
                })
                // publishOn will execute / take action from here
                .publishOn(Schedulers.single())
                .map(i -> {
                    log.info("printing from map-2, thread == {}", Thread.currentThread().getName());
                    return i;
                });

        StepVerifier.create(subscribeOnPublisher)
                .expectNextCount(5)
                .verifyComplete();

    }


    @Test
    void subscribeOnIO() {
        Mono<List<String>> makeAnListOfString = Mono.fromCallable(() -> Files.readAllLines(Path.of("textfile.txt")))
                .log()
                .subscribeOn(Schedulers.single());

                makeAnListOfString.subscribe();

        StepVerifier.create(makeAnListOfString)
                .expectSubscription()
                .thenConsumeWhile((k)->{
                    Assertions.assertFalse(k.isEmpty());
                    return true;
                })
                .verifyComplete();

    }

    // this method is running infinity loop
    @Test
    void switchIfEmpty(){
        Flux<Object>emptyFlux=Flux.empty()
                .switchIfEmpty(i->log.info("Flux is empty"))
                .log();

        StepVerifier.create(emptyFlux)
                .expectNext("Flux is empty")
                .expectComplete()
                .verify();
    }


    @Test
    public void deferOperation() {
        Mono<Long> defer = Mono.defer(() -> Mono.just(System.currentTimeMillis()));
        defer.subscribe(s -> log.info("{}", s));

        AtomicLong atomicLong = new AtomicLong(0);
        defer.subscribe(atomicLong::set);
        Assertions.assertTrue(atomicLong.get() > 0);

    }


    @Test
    public void concatOperator() {
        Flux<String> f1 = Flux.just("a", "b");
        Flux<String> f2 = Flux.just("c", "d");
        Flux<String> log = Flux.concat(f1, f2).log();
        StepVerifier.create(log)
                .expectSubscription()
                .expectNext("a", "b", "c", "d")
                .expectComplete()
                .verify();
    }




}
